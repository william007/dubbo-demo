/**
 * 
 */
package org.dubbo.demo.consumer;

import java.io.IOException;

import org.dubbo.api.DemoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/** 
 * Description: 
 * @author xuhongwei
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * 2018年8月29日    xuhongwei     1.0        1.0 Version 
 * </pre>
 */
public class Consumer {
	
	
	public static void main(String[] args) throws Exception {
		
		
		ClassPathXmlApplicationContext  context = new ClassPathXmlApplicationContext(new String[]{"dubbo-demo-consumer.xml"});
		
		context.start(); 
	
		DemoService demoService = (DemoService) context.getBean("demoService");
		
		String result = demoService.sayHello("jim");
		
		System.out.println(result);
		
		System.in.read();
		
	}
}
