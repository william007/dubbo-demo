/**
 * 
 */
package org.dubbo.demo.provider;

import org.dubbo.api.DemoService;

/** 
 * Description: 
 * @author xuhongwei
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * 2018年8月29日    xuhongwei     1.0        1.0 Version 
 * </pre>
 */
public class DemoServiceImpl  implements  DemoService{
	
	
	/**
	 * Description: 
	 * @param
	 * @return 
	 * @throws
	 * @Author xuhongwei
	 * Create Date: 2018年8月29日 上午10:54:44
	 */
	public String sayHello(String name) {
		// TODO Auto-generated method stub
		return  "Hello "+ name ;
	}
	
	
}	
