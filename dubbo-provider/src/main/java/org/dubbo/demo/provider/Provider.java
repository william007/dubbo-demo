/**
 * 
 */
package org.dubbo.demo.provider;


import org.springframework.context.support.ClassPathXmlApplicationContext;

/** 
 * Description: 
 * @author xuhongwei
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * 2018年8月29日    xuhongwei     1.0        1.0 Version 
 * </pre>
 */
public class Provider {
	
	public static void main(String[] args) throws Exception {
		System.setProperty("java.net.preferIPv4Stack", "true");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"META-INF/dubbo-demo-provider.xml"});		
		context.start();
		System.out.println("Provider started.");
		System.in.read();
	}
}
