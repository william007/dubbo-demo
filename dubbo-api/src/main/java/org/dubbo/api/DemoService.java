/**
 * 
 */
package org.dubbo.api;

/** 
 * Description: 
 * @author xuhongwei
 * @version 1.0
 * <pre>
 * Modification History: 
 * Date         Author      Version     Description 
------------------------------------------------------------------
 * 2018年8月29日    xuhongwei     1.0        1.0 Version 
 * </pre>
 */
public interface DemoService {
	
	public  String sayHello(String name);
	
}
